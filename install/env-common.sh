#!/bin/sh
#
# This script will be executed in the current shell (not in a subshell as the other scripts), so it is ok to define common shared variable here.
#

export APACHE_ORIG="http://www-eu.apache.org/dist"
export APACHE_MIRROR="http://mirror.hosting90.cz/apache"
